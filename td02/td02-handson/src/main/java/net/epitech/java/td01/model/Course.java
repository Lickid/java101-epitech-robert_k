package net.epitech.java.td01.model;

import org.joda.time.DateTime;

public class Course
{
	public String get_module() {
		return _module;
	}
	public void set_module(String _module) {
		this._module = _module;
	}
	public String get_activity() {
		return _activity;
	}
	public void set_activity(String _activity) {
		this._activity = _activity;
	}
	public DateTime get_date() {
		return _date;
	}
	public void set_date(DateTime _date) {
		this._date = _date;
	}
	public Teacher get_teacher() {
		return _teacher;
	}
	public void set_teacher(Teacher _teacher) {
		this._teacher = _teacher;
	}
	public State get_state() {
		return _state;
	}
	public void set_state(State _state) {
		this._state = _state;
	}
	
	//TODO: create public enum
	public enum State
	{
		FREE,
		BUSY;
	}
	private String		_module;
	private String		_activity;
	private DateTime	_date;
	private Teacher		_teacher;
	private State		_state;
}
